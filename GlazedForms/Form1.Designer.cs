﻿namespace GlazedForms
{
    partial class FiberGlass
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FiberGlass));
            this.comboBoxMaterial = new System.Windows.Forms.ComboBox();
            this.label_material = new System.Windows.Forms.Label();
            this.Calculate = new System.Windows.Forms.Button();
            this.WindowSizes = new System.Windows.Forms.Label();
            this.label_width = new System.Windows.Forms.Label();
            this.label_heigth = new System.Windows.Forms.Label();
            this.width_textBox = new System.Windows.Forms.TextBox();
            this.height_textBox = new System.Windows.Forms.TextBox();
            this.label_fiberglass = new System.Windows.Forms.Label();
            this.radioButtonOneChamber = new System.Windows.Forms.RadioButton();
            this.radioButtonTwoChamber = new System.Windows.Forms.RadioButton();
            this.checkBoxWindowsill = new System.Windows.Forms.CheckBox();
            this.linkLabelNameSurname = new System.Windows.Forms.LinkLabel();
            this.result = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // comboBoxMaterial
            // 
            this.comboBoxMaterial.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxMaterial.FormattingEnabled = true;
            this.comboBoxMaterial.Items.AddRange(new object[] {
            resources.GetString("comboBoxMaterial.Items"),
            resources.GetString("comboBoxMaterial.Items1"),
            resources.GetString("comboBoxMaterial.Items2")});
            resources.ApplyResources(this.comboBoxMaterial, "comboBoxMaterial");
            this.comboBoxMaterial.Name = "comboBoxMaterial";
            // 
            // label_material
            // 
            resources.ApplyResources(this.label_material, "label_material");
            this.label_material.Name = "label_material";
            // 
            // Calculate
            // 
            resources.ApplyResources(this.Calculate, "Calculate");
            this.Calculate.Name = "Calculate";
            this.Calculate.UseVisualStyleBackColor = true;
            this.Calculate.Click += new System.EventHandler(this.Calculate_Click);
            // 
            // WindowSizes
            // 
            resources.ApplyResources(this.WindowSizes, "WindowSizes");
            this.WindowSizes.Name = "WindowSizes";
            // 
            // label_width
            // 
            resources.ApplyResources(this.label_width, "label_width");
            this.label_width.Name = "label_width";
            // 
            // label_heigth
            // 
            resources.ApplyResources(this.label_heigth, "label_heigth");
            this.label_heigth.Name = "label_heigth";
            // 
            // width_textBox
            // 
            resources.ApplyResources(this.width_textBox, "width_textBox");
            this.width_textBox.Name = "width_textBox";
            this.width_textBox.TextChanged += new System.EventHandler(this.width_textBox_TextChanged);
            this.width_textBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.width_textBox_KeyPress);
            // 
            // height_textBox
            // 
            resources.ApplyResources(this.height_textBox, "height_textBox");
            this.height_textBox.Name = "height_textBox";
            this.height_textBox.TextChanged += new System.EventHandler(this.height_textBox_TextChanged);
            this.height_textBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.height_textBox_KeyPress);
            // 
            // label_fiberglass
            // 
            resources.ApplyResources(this.label_fiberglass, "label_fiberglass");
            this.label_fiberglass.Name = "label_fiberglass";
            // 
            // radioButtonOneChamber
            // 
            resources.ApplyResources(this.radioButtonOneChamber, "radioButtonOneChamber");
            this.radioButtonOneChamber.Checked = true;
            this.radioButtonOneChamber.Name = "radioButtonOneChamber";
            this.radioButtonOneChamber.TabStop = true;
            this.radioButtonOneChamber.UseVisualStyleBackColor = true;
            // 
            // radioButtonTwoChamber
            // 
            resources.ApplyResources(this.radioButtonTwoChamber, "radioButtonTwoChamber");
            this.radioButtonTwoChamber.Name = "radioButtonTwoChamber";
            this.radioButtonTwoChamber.UseVisualStyleBackColor = true;
            // 
            // checkBoxWindowsill
            // 
            resources.ApplyResources(this.checkBoxWindowsill, "checkBoxWindowsill");
            this.checkBoxWindowsill.Name = "checkBoxWindowsill";
            this.checkBoxWindowsill.UseVisualStyleBackColor = true;
            // 
            // linkLabelNameSurname
            // 
            resources.ApplyResources(this.linkLabelNameSurname, "linkLabelNameSurname");
            this.linkLabelNameSurname.Name = "linkLabelNameSurname";
            this.linkLabelNameSurname.TabStop = true;
            // 
            // result
            // 
            resources.ApplyResources(this.result, "result");
            this.result.ForeColor = System.Drawing.Color.DarkRed;
            this.result.Name = "result";
            // 
            // FiberGlass
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.result);
            this.Controls.Add(this.linkLabelNameSurname);
            this.Controls.Add(this.checkBoxWindowsill);
            this.Controls.Add(this.radioButtonTwoChamber);
            this.Controls.Add(this.radioButtonOneChamber);
            this.Controls.Add(this.label_fiberglass);
            this.Controls.Add(this.height_textBox);
            this.Controls.Add(this.width_textBox);
            this.Controls.Add(this.label_heigth);
            this.Controls.Add(this.label_width);
            this.Controls.Add(this.WindowSizes);
            this.Controls.Add(this.Calculate);
            this.Controls.Add(this.label_material);
            this.Controls.Add(this.comboBoxMaterial);
            this.Name = "FiberGlass";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox comboBoxMaterial;
        private System.Windows.Forms.Label label_material;
        private System.Windows.Forms.Button Calculate;
        private System.Windows.Forms.Label WindowSizes;
        private System.Windows.Forms.Label label_width;
        private System.Windows.Forms.Label label_heigth;
        private System.Windows.Forms.TextBox width_textBox;
        private System.Windows.Forms.TextBox height_textBox;
        private System.Windows.Forms.Label label_fiberglass;
        private System.Windows.Forms.RadioButton radioButtonOneChamber;
        private System.Windows.Forms.RadioButton radioButtonTwoChamber;
        private System.Windows.Forms.CheckBox checkBoxWindowsill;
        private System.Windows.Forms.LinkLabel linkLabelNameSurname;
        private System.Windows.Forms.Label result;
    }
}

