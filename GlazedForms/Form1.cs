﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GlazedForms
{
    public partial class FiberGlass : Form
    {
        public FiberGlass()
        {
            InitializeComponent();
            radioButtonOneChamber.Checked = true;
            Calculate.Enabled = false;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            System.Globalization.CultureInfo customCulture = (System.Globalization.CultureInfo)
            System.Threading.Thread.CurrentThread.CurrentCulture.Clone();
            customCulture.NumberFormat.NumberDecimalSeparator = ".";
            System.Threading.Thread.CurrentThread.CurrentCulture = customCulture;

            comboBoxMaterial.SelectedIndex = 0;
        }

        private void Calculate_Click(object sender, EventArgs e)
        {
            double width = double.Parse(width_textBox.Text);
            double height = double.Parse(height_textBox.Text);
            double costDoubleGlazedWindows;
            double price = 0;
            int priceWindowsill = 0;
            if (radioButtonOneChamber.Checked && comboBoxMaterial.SelectedIndex == 0)
            {
                price = 0.25;
            }

            if (radioButtonOneChamber.Checked && comboBoxMaterial.SelectedIndex == 1)
            {
                price = 0.30;
            }

            if (radioButtonOneChamber.Checked && comboBoxMaterial.SelectedIndex == 2)
            {
                price = 0.05;
            }

            if (radioButtonTwoChamber.Checked && comboBoxMaterial.SelectedIndex == 0)
            {
                price = 0.10;
            }

            if (radioButtonTwoChamber.Checked && comboBoxMaterial.SelectedIndex == 1)
            {
                price = 0.15;
            }

            if (radioButtonTwoChamber.Checked && comboBoxMaterial.SelectedIndex == 2)
            {
                price = 0.20;
            }

            if (checkBoxWindowsill.Checked)
            {
                priceWindowsill = 35;
            }

            costDoubleGlazedWindows = width * height * price + priceWindowsill;

            result.Text = "Вартість: " + costDoubleGlazedWindows.ToString();
        }

        private void width_textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void height_textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
        }

        private void width_textBox_TextChanged(object sender, EventArgs e)
        {
            if(width_textBox.Text.Length == 0 && height_textBox.Text.Length == 0)
            {
                Calculate.Enabled = false;
            }
            else if(width_textBox.Text.Length == 0)
            {
                Calculate.Enabled = false;
            }
            else if (height_textBox.Text.Length == 0)
            {
                Calculate.Enabled = false;
            }
            else
            {
                Calculate.Enabled = true;
            }
            result.Text = "";

        }

        private void height_textBox_TextChanged(object sender, EventArgs e)
        {
            if (width_textBox.Text.Length == 0 && height_textBox.Text.Length == 0)
            {
                Calculate.Enabled = false;
            }
            else if (width_textBox.Text.Length == 0)
            {
                Calculate.Enabled = false;
            }
            else if (height_textBox.Text.Length == 0)
            {
                Calculate.Enabled = false;
            }
            else
            {
                Calculate.Enabled = true;
            }
            result.Text = "";

        }
    }
}
