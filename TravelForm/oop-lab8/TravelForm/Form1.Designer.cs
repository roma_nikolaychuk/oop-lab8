﻿namespace TravelForm
{
    partial class TravelForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.NumberOfTrips = new System.Windows.Forms.Label();
            this.textBox_kilkist = new System.Windows.Forms.TextBox();
            this.Country = new System.Windows.Forms.Label();
            this.Season = new System.Windows.Forms.Label();
            this.NameSurnameGroup = new System.Windows.Forms.LinkLabel();
            this.comboBox_season = new System.Windows.Forms.ComboBox();
            this.radioButtonBolgariya = new System.Windows.Forms.RadioButton();
            this.radioButtonGerman = new System.Windows.Forms.RadioButton();
            this.radioButtonPolish = new System.Windows.Forms.RadioButton();
            this.checkBoxIndividual = new System.Windows.Forms.CheckBox();
            this.buttonResult = new System.Windows.Forms.Button();
            this.labelResult = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // NumberOfTrips
            // 
            this.NumberOfTrips.AutoSize = true;
            this.NumberOfTrips.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.NumberOfTrips.Location = new System.Drawing.Point(20, 20);
            this.NumberOfTrips.Name = "NumberOfTrips";
            this.NumberOfTrips.Size = new System.Drawing.Size(121, 17);
            this.NumberOfTrips.TabIndex = 0;
            this.NumberOfTrips.Text = "Кількість путівок:";
            // 
            // textBox_kilkist
            // 
            this.textBox_kilkist.Location = new System.Drawing.Point(140, 20);
            this.textBox_kilkist.Name = "textBox_kilkist";
            this.textBox_kilkist.Size = new System.Drawing.Size(50, 20);
            this.textBox_kilkist.TabIndex = 1;
            this.textBox_kilkist.TextChanged += new System.EventHandler(this.textBox_kilkist_TextChanged);
            this.textBox_kilkist.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_kilkist_KeyPress);
            // 
            // Country
            // 
            this.Country.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Country.AutoSize = true;
            this.Country.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.Country.Location = new System.Drawing.Point(275, 20);
            this.Country.Name = "Country";
            this.Country.Size = new System.Drawing.Size(52, 17);
            this.Country.TabIndex = 2;
            this.Country.Text = "Країна";
            // 
            // Season
            // 
            this.Season.AutoSize = true;
            this.Season.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.Season.Location = new System.Drawing.Point(20, 65);
            this.Season.Name = "Season";
            this.Season.Size = new System.Drawing.Size(80, 17);
            this.Season.TabIndex = 3;
            this.Season.Text = "Пора року:";
            // 
            // NameSurnameGroup
            // 
            this.NameSurnameGroup.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.NameSurnameGroup.AutoSize = true;
            this.NameSurnameGroup.Location = new System.Drawing.Point(237, 239);
            this.NameSurnameGroup.Name = "NameSurnameGroup";
            this.NameSurnameGroup.Size = new System.Drawing.Size(135, 13);
            this.NameSurnameGroup.TabIndex = 4;
            this.NameSurnameGroup.TabStop = true;
            this.NameSurnameGroup.Text = "Ніколайчук Роман, СН-21";
            // 
            // comboBox_season
            // 
            this.comboBox_season.FormattingEnabled = true;
            this.comboBox_season.Items.AddRange(new object[] {
            "Літо",
            "Зима"});
            this.comboBox_season.Location = new System.Drawing.Point(106, 65);
            this.comboBox_season.Name = "comboBox_season";
            this.comboBox_season.Size = new System.Drawing.Size(84, 21);
            this.comboBox_season.TabIndex = 5;
            // 
            // radioButtonBolgariya
            // 
            this.radioButtonBolgariya.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioButtonBolgariya.AutoSize = true;
            this.radioButtonBolgariya.Checked = true;
            this.radioButtonBolgariya.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.radioButtonBolgariya.Location = new System.Drawing.Point(275, 65);
            this.radioButtonBolgariya.Name = "radioButtonBolgariya";
            this.radioButtonBolgariya.Size = new System.Drawing.Size(83, 21);
            this.radioButtonBolgariya.TabIndex = 6;
            this.radioButtonBolgariya.TabStop = true;
            this.radioButtonBolgariya.Text = "Болгарія";
            this.radioButtonBolgariya.UseVisualStyleBackColor = true;
            // 
            // radioButtonGerman
            // 
            this.radioButtonGerman.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioButtonGerman.AutoSize = true;
            this.radioButtonGerman.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.radioButtonGerman.Location = new System.Drawing.Point(275, 100);
            this.radioButtonGerman.Name = "radioButtonGerman";
            this.radioButtonGerman.Size = new System.Drawing.Size(96, 21);
            this.radioButtonGerman.TabIndex = 7;
            this.radioButtonGerman.TabStop = true;
            this.radioButtonGerman.Text = "Німеччина";
            this.radioButtonGerman.UseVisualStyleBackColor = true;
            // 
            // radioButtonPolish
            // 
            this.radioButtonPolish.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.radioButtonPolish.AutoSize = true;
            this.radioButtonPolish.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.radioButtonPolish.Location = new System.Drawing.Point(275, 135);
            this.radioButtonPolish.Name = "radioButtonPolish";
            this.radioButtonPolish.Size = new System.Drawing.Size(78, 21);
            this.radioButtonPolish.TabIndex = 8;
            this.radioButtonPolish.TabStop = true;
            this.radioButtonPolish.Text = "Польща";
            this.radioButtonPolish.UseVisualStyleBackColor = true;
            // 
            // checkBoxIndividual
            // 
            this.checkBoxIndividual.AutoSize = true;
            this.checkBoxIndividual.Location = new System.Drawing.Point(23, 135);
            this.checkBoxIndividual.Name = "checkBoxIndividual";
            this.checkBoxIndividual.Size = new System.Drawing.Size(174, 17);
            this.checkBoxIndividual.TabIndex = 9;
            this.checkBoxIndividual.Text = "Вартість індивідуального гіда";
            this.checkBoxIndividual.UseVisualStyleBackColor = true;
            // 
            // buttonResult
            // 
            this.buttonResult.Location = new System.Drawing.Point(23, 175);
            this.buttonResult.Name = "buttonResult";
            this.buttonResult.Size = new System.Drawing.Size(167, 23);
            this.buttonResult.TabIndex = 10;
            this.buttonResult.Text = "Розрахувати";
            this.buttonResult.UseVisualStyleBackColor = true;
            this.buttonResult.Click += new System.EventHandler(this.buttonResult_Click);
            // 
            // labelResult
            // 
            this.labelResult.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelResult.AutoSize = true;
            this.labelResult.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F);
            this.labelResult.ForeColor = System.Drawing.Color.DarkRed;
            this.labelResult.Location = new System.Drawing.Point(237, 185);
            this.labelResult.Name = "labelResult";
            this.labelResult.Size = new System.Drawing.Size(0, 24);
            this.labelResult.TabIndex = 11;
            // 
            // TravelForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 261);
            this.Controls.Add(this.labelResult);
            this.Controls.Add(this.buttonResult);
            this.Controls.Add(this.checkBoxIndividual);
            this.Controls.Add(this.radioButtonPolish);
            this.Controls.Add(this.radioButtonGerman);
            this.Controls.Add(this.radioButtonBolgariya);
            this.Controls.Add(this.comboBox_season);
            this.Controls.Add(this.NameSurnameGroup);
            this.Controls.Add(this.Season);
            this.Controls.Add(this.Country);
            this.Controls.Add(this.textBox_kilkist);
            this.Controls.Add(this.NumberOfTrips);
            this.MaximumSize = new System.Drawing.Size(400, 300);
            this.MinimumSize = new System.Drawing.Size(350, 250);
            this.Name = "TravelForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TravelForm";
            this.Load += new System.EventHandler(this.TravelForm_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label NumberOfTrips;
        private System.Windows.Forms.TextBox textBox_kilkist;
        private System.Windows.Forms.Label Country;
        private System.Windows.Forms.Label Season;
        private System.Windows.Forms.LinkLabel NameSurnameGroup;
        private System.Windows.Forms.ComboBox comboBox_season;
        private System.Windows.Forms.RadioButton radioButtonBolgariya;
        private System.Windows.Forms.RadioButton radioButtonGerman;
        private System.Windows.Forms.RadioButton radioButtonPolish;
        private System.Windows.Forms.CheckBox checkBoxIndividual;
        private System.Windows.Forms.Button buttonResult;
        private System.Windows.Forms.Label labelResult;
    }
}

