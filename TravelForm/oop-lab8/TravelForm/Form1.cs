﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace TravelForm
{
    public partial class TravelForm : Form
    {
        public TravelForm()
        {
            InitializeComponent();
            radioButtonBolgariya.Checked = true;
            buttonResult.Enabled = false;
        }

        private void TravelForm_Load(object sender, EventArgs e)
        {
            comboBox_season.SelectedIndex = 0;
        }

        private void buttonResult_Click(object sender, EventArgs e)
        {
            int NumberOfTrips = int.Parse(textBox_kilkist.Text);
            int CostOfTheTourOrder;
            int price = 0;
            int priceIndividual = 0;
            if (radioButtonBolgariya.Checked && comboBox_season.SelectedIndex == 0)
            {
                price = 100;
            }

            if (radioButtonBolgariya.Checked && comboBox_season.SelectedIndex == 1)
            {
                price = 150;
            }

            if (radioButtonGerman.Checked && comboBox_season.SelectedIndex == 0)
            {
                price = 160;
            }

            if (radioButtonGerman.Checked && comboBox_season.SelectedIndex == 1)
            {
                price = 200;
            }

            if (radioButtonPolish.Checked && comboBox_season.SelectedIndex == 0)
            {
                price = 120;
            }

            if (radioButtonPolish.Checked && comboBox_season.SelectedIndex == 1)
            {
                price = 180;
            }

            if (checkBoxIndividual.Checked)
            {
                priceIndividual = 50;
            }

            CostOfTheTourOrder = NumberOfTrips * price + priceIndividual;
            labelResult.Text = "Вартість: " + CostOfTheTourOrder.ToString() + "$";
        }

        private void textBox_kilkist_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }

        }

        private void textBox_kilkist_TextChanged(object sender, EventArgs e)
        {
            if (textBox_kilkist.Text.Length == 0)
            {
                buttonResult.Enabled = false;
            }
            else
            {
                buttonResult.Enabled = true;
            }
            labelResult.Text = "";
        }
    }
}
